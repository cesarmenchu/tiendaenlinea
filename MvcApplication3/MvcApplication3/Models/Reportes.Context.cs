﻿//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcApplication3.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class fabricaEntities3 : DbContext
    {
        public fabricaEntities3()
            : base("name=fabricaEntities3")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<VistaConsulta11> VistaConsulta11 { get; set; }
        public DbSet<VistaConsulta12> VistaConsulta12 { get; set; }
        public DbSet<VistaConsulta21> VistaConsulta21 { get; set; }
        public DbSet<VistaConsulta22> VistaConsulta22 { get; set; }
        public DbSet<VistaConsulta3> VistaConsulta3 { get; set; }
        public DbSet<VistaConsulta4> VistaConsulta4 { get; set; }
        public DbSet<VistaConsulta5> VistaConsulta5 { get; set; }
        public DbSet<VistaConsulta6> VistaConsulta6 { get; set; }
        public DbSet<VistaConsulta7> VistaConsulta7 { get; set; }
    }
}
