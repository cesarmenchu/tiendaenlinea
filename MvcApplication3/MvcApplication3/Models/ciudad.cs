//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcApplication3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ciudad
    {
        public ciudad()
        {
            this.distribuidora = new HashSet<distribuidora>();
            this.vendedor = new HashSet<vendedor>();
        }
    
        public decimal codigo { get; set; }
        public string nombre { get; set; }
        public string pais_nombre { get; set; }
    
        public virtual pais pais { get; set; }
        public virtual ICollection<distribuidora> distribuidora { get; set; }
        public virtual ICollection<vendedor> vendedor { get; set; }
    }
}
