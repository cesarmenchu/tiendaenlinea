﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication3.Models;

namespace MvcApplication3.Controllers
{
    public class DistribuidoraController : Controller
    {
        private fabricaEntities1 db = new fabricaEntities1();

        //
        // GET: /Distribuidora/

        public ActionResult Index()
        {
            var distribuidora = db.distribuidora.Include(d => d.ciudad).Include(d => d.tipodistribuidora);
            return View(distribuidora.ToList());
        }

        //
        // GET: /Distribuidora/Details/5

        public ActionResult Details(string id = null)
        {
            distribuidora distribuidora = db.distribuidora.Find(id);
            if (distribuidora == null)
            {
                return HttpNotFound();
            }
            return View(distribuidora);
        }

        //
        // GET: /Distribuidora/Create

        public ActionResult Create()
        {
            ViewBag.ciudad_nombre = new SelectList(db.ciudad, "nombre", "pais_nombre");
            ViewBag.tipodistribuidora_nombre = new SelectList(db.tipodistribuidora, "nombre", "nombre");
            return View();
        }

        //
        // POST: /Distribuidora/Create

        [HttpPost]
        public ActionResult Create(distribuidora distribuidora)
        {
            if (ModelState.IsValid)
            {
                db.distribuidora.Add(distribuidora);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ciudad_nombre = new SelectList(db.ciudad, "nombre", "pais_nombre", distribuidora.ciudad_nombre);
            ViewBag.tipodistribuidora_nombre = new SelectList(db.tipodistribuidora, "nombre", "nombre", distribuidora.tipodistribuidora_nombre);
            return View(distribuidora);
        }

        //
        // GET: /Distribuidora/Edit/5

        public ActionResult Edit(string id = null)
        {
            distribuidora distribuidora = db.distribuidora.Find(id);
            if (distribuidora == null)
            {
                return HttpNotFound();
            }
            ViewBag.ciudad_nombre = new SelectList(db.ciudad, "nombre", "pais_nombre", distribuidora.ciudad_nombre);
            ViewBag.tipodistribuidora_nombre = new SelectList(db.tipodistribuidora, "nombre", "nombre", distribuidora.tipodistribuidora_nombre);
            return View(distribuidora);
        }

        //
        // POST: /Distribuidora/Edit/5

        [HttpPost]
        public ActionResult Edit(distribuidora distribuidora)
        {
            if (ModelState.IsValid)
            {
                db.Entry(distribuidora).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ciudad_nombre = new SelectList(db.ciudad, "nombre", "pais_nombre", distribuidora.ciudad_nombre);
            ViewBag.tipodistribuidora_nombre = new SelectList(db.tipodistribuidora, "nombre", "nombre", distribuidora.tipodistribuidora_nombre);
            return View(distribuidora);
        }

        //
        // GET: /Distribuidora/Delete/5

        public ActionResult Delete(string id = null)
        {
            distribuidora distribuidora = db.distribuidora.Find(id);
            if (distribuidora == null)
            {
                return HttpNotFound();
            }
            return View(distribuidora);
        }

        //
        // POST: /Distribuidora/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            distribuidora distribuidora = db.distribuidora.Find(id);
            db.distribuidora.Remove(distribuidora);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}