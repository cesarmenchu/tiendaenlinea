﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication3.Models;

namespace MvcApplication3.Controllers
{
    public class DescripcionController : Controller
    {
        private fabricaEntities1 db = new fabricaEntities1();

        //
        // GET: /Descripcion/

        public ActionResult Index()
        {
            var descripcion_pedido = db.descripcion_pedido.Include(d => d.pedido).Include(d => d.producto).Include(d => d.distribuidora);
            return View(descripcion_pedido.ToList());
        }

        //
        // GET: /Descripcion/Details/5

        public ActionResult Details(decimal id = 0)
        {
            descripcion_pedido descripcion_pedido = db.descripcion_pedido.Find(id);
            if (descripcion_pedido == null)
            {
                return HttpNotFound();
            }
            return View(descripcion_pedido);
        }

        //
        // GET: /Descripcion/Create

        public ActionResult Create()
        {
            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "fecha");
            ViewBag.pedido_tienda_nombre = new SelectList(db.pedido, "tienda_nombre", "tienda_nombre");
            ViewBag.producto_nombre = new SelectList(db.producto, "nombre", "nombre");
            ViewBag.distribuidora_nombre = new SelectList(db.distribuidora, "nombre", "nombre");
            return View();
        }

        //
        // POST: /Descripcion/Create

        [HttpPost]
        public ActionResult Create(descripcion_pedido descripcion_pedido)
        {
            if (ModelState.IsValid)
            {
                db.descripcion_pedido.Add(descripcion_pedido);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "fecha", descripcion_pedido.pedido_fecha);
            ViewBag.pedido_tienda_nombre = new SelectList(db.pedido, "tienda_nombre", "tienda_nombre", descripcion_pedido.pedido_tienda_nombre);
            ViewBag.producto_nombre = new SelectList(db.producto, "nombre_producto", "nombre_producto", descripcion_pedido.producto_nombre);
            ViewBag.distribuidora_nombre = new SelectList(db.distribuidora, "nombre_distribuidora", "nombre_distribuidora", descripcion_pedido.distribuidora_nombre);
            return View(descripcion_pedido);
        }

        //
        // GET: /Descripcion/Edit/5

        public ActionResult Edit(decimal id = 0)
        {
            descripcion_pedido descripcion_pedido = db.descripcion_pedido.Find(id);
            if (descripcion_pedido == null)
            {
                return HttpNotFound();
            }
            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "tienda_nombre", descripcion_pedido.pedido_fecha);
            ViewBag.producto_nombre = new SelectList(db.producto, "nombre", "categoria_nombre", descripcion_pedido.producto_nombre);
            ViewBag.distribuidora_nombre = new SelectList(db.distribuidora, "nombre", "ciudad_nombre", descripcion_pedido.distribuidora_nombre);
            return View(descripcion_pedido);
        }

        //
        // POST: /Descripcion/Edit/5

        [HttpPost]
        public ActionResult Edit(descripcion_pedido descripcion_pedido)
        {
            if (ModelState.IsValid)
            {
                db.Entry(descripcion_pedido).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "tienda_nombre", descripcion_pedido.pedido_fecha);
            ViewBag.producto_nombre = new SelectList(db.producto, "nombre", "categoria_nombre", descripcion_pedido.producto_nombre);
            ViewBag.distribuidora_nombre = new SelectList(db.distribuidora, "nombre", "ciudad_nombre", descripcion_pedido.distribuidora_nombre);
            return View(descripcion_pedido);
        }

        //
        // GET: /Descripcion/Delete/5

        public ActionResult Delete(decimal id = 0)
        {
            descripcion_pedido descripcion_pedido = db.descripcion_pedido.Find(id);
            if (descripcion_pedido == null)
            {
                return HttpNotFound();
            }
            return View(descripcion_pedido);
        }

        //
        // POST: /Descripcion/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(decimal id)
        {
            descripcion_pedido descripcion_pedido = db.descripcion_pedido.Find(id);
            db.descripcion_pedido.Remove(descripcion_pedido);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}