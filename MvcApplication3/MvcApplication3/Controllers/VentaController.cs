﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication3.Models;

namespace MvcApplication3.Controllers
{
    public class VentaController : Controller
    {
        private fabricaEntities1 db = new fabricaEntities1();

        //
        // GET: /Venta/

        public ActionResult Index()
        {
            var venta = db.venta.Include(v => v.pedido).Include(v => v.vendedor);
            return View(venta.ToList());
        }

        //
        // GET: /Venta/Details/5

        public ActionResult Details(decimal id = 0)
        {
            venta venta = db.venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        //
        // GET: /Venta/Create

        public ActionResult Create()
        {
            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "fecha");
            ViewBag.pedido_tienda_nombre = new SelectList(db.pedido, "tienda_nombre", "tienda_nombre");
            ViewBag.vendedor_nombre = new SelectList(db.vendedor, "nombre", "nombre");
            return View();
        }

        //
        // POST: /Venta/Create

        [HttpPost]
        public ActionResult Create(venta venta)
        {
            if (ModelState.IsValid)
            {
                db.venta.Add(venta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "fecha", venta.pedido_fecha);
            ViewBag.pedido_tienda_nombre = new SelectList(db.pedido, "tienda_nombre", "tienda_nombre", venta.pedido_tienda_nombre);
            ViewBag.vendedor_nombre = new SelectList(db.vendedor, "nombre", "nombre", venta.vendedor_nombre);
            return View(venta);
        }

        //
        // GET: /Venta/Edit/5

        public ActionResult Edit(decimal id = 0)
        {
            venta venta = db.venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "tienda_nombre", venta.pedido_fecha);
            ViewBag.vendedor_nombre = new SelectList(db.vendedor, "nombre", "ciudad_nombre", venta.vendedor_nombre);
            return View(venta);
        }

        //
        // POST: /Venta/Edit/5

        [HttpPost]
        public ActionResult Edit(venta venta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(venta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pedido_fecha = new SelectList(db.pedido, "fecha", "tienda_nombre", venta.pedido_fecha);
            ViewBag.vendedor_nombre = new SelectList(db.vendedor, "nombre", "ciudad_nombre", venta.vendedor_nombre);
            return View(venta);
        }

        //
        // GET: /Venta/Delete/5

        public ActionResult Delete(decimal id = 0)
        {
            venta venta = db.venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        //
        // POST: /Venta/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(decimal id)
        {
            venta venta = db.venta.Find(id);
            db.venta.Remove(venta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}