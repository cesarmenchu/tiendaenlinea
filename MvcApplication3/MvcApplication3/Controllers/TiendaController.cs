﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication3.Models;

namespace MvcApplication3.Controllers
{
    public class TiendaController : Controller
    {
        private fabricaEntities1 db = new fabricaEntities1();

        //
        // GET: /Tienda/

        public ActionResult Index()
        {
            return View(db.tienda.ToList());
        }

        //
        // GET: /Tienda/Details/5

        public ActionResult Details(string id = null)
        {
            tienda tienda = db.tienda.Find(id);
            if (tienda == null)
            {
                return HttpNotFound();
            }
            return View(tienda);
        }

        //
        // GET: /Tienda/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Tienda/Create

        [HttpPost]
        public ActionResult Create(tienda tienda)
        {
            if (ModelState.IsValid)
            {
                db.tienda.Add(tienda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tienda);
        }

        //
        // GET: /Tienda/Edit/5

        public ActionResult Edit(string id = null)
        {
            tienda tienda = db.tienda.Find(id);
            if (tienda == null)
            {
                return HttpNotFound();
            }
            return View(tienda);
        }

        //
        // POST: /Tienda/Edit/5

        [HttpPost]
        public ActionResult Edit(tienda tienda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tienda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tienda);
        }

        //
        // GET: /Tienda/Delete/5

        public ActionResult Delete(string id = null)
        {
            tienda tienda = db.tienda.Find(id);
            if (tienda == null)
            {
                return HttpNotFound();
            }
            return View(tienda);
        }

        //
        // POST: /Tienda/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            tienda tienda = db.tienda.Find(id);
            db.tienda.Remove(tienda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}